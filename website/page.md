<nav>

- [<span>![](./logo-symbolic.svg) **GNOME Feeds**</span>](#)
- [Features](#features)
- [Install](#install)
- [Hack](#hack)
- [Support](#support)

</nav>

<!--@MARGIN@-->

![Logo](logo.svg)

# GNOME Feeds

## News reader for GNOME

![Main Window](screenshots/mainwindow.png)

---

## Features

### Simple User Experience

Add your favorite feeds, and start reading the latest news.

![Add](screenshots/add_feed.png)

### Easy on the eyes

Cut the clutter of modern websites by using the reader mode. Prefer a standard web view? Sure thing, we disabled JavaScript for you. No more distractions!

![Reader](screenshots/reader.png)

### Large feed collection? Import it

GNOME Feeds supports importing and exporting your feed collection from and to OPML so that you can get set up in seconds.

![Import and Export](screenshots/import_export.png)

### Responsive

GNOME Feeds is built with Purism's libhandy library to offer a responsive user interface. Got a new shiny GNU+Linux phone? Here's a feed reader for you!

<video autoplay mute controls loop>
    <source src="screenshots/responsive_demo.mp4" type="video/mp4" />
</video>

---

## Install

### Flatpak (recommended)

[Install **Flatpak** by following the quick setup guide](https://flatpak.org/setup/). Does it work on your distribution? Most likely.

Then, click the button below to install GNOME Feeds:

<a href="https://flathub.org/apps/details/org.gabmus.gnome-feeds"><img src="https://raw.githubusercontent.com/flatpak-design-team/flathub-mockups/master/assets/download-button/download.svg?sanitize=true" height="100" alt="Get it on Flathub" /></a>

<!-- ### AUR

If you're using Arch Linux or an Arch based system, you can install the [`gnome-feeds-git`](https://aur.archlinux.org/packages/gnome-feeds-git/) package from the AUR. -->

---

## Hack

![Code](icons/code.svg)

GNOME Feeds is written using Python 3 and GTK+ 3. It's free software, released under the GPL3 license. Feel free to browse the source code on [the GitLab repository](https://gitlab.com/gabmus/gnome-feeds), fork it, make changes or open issues!

---

## Support

![Bug](icons/bug.svg)

Have you found a bug? Do you want a new feature? Whatever the case, opening an issue is never a bad idea. You can do that on [the issue page of GNOME Feeds' GitLab repository](https://gitlab.com/gabmus/gnome-feeds/issues)
